from django.conf.urls import url
from .views import *
from django.urls import re_path

app_name = "users"
urlpatterns = [
    re_path('^employee/$', SearchEmployee.as_view()),
]
